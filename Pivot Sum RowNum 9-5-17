WITH CTE_CP AS
(Select DISTINCT
         C.CONTACT_ID
       , C.FIRSTNAME
       , C.EMAIL
       , CP.COMMUNICATION_TYPE_ID
       , CASE WHEN CP.COMMUNICATION_TYPE_ID != 1
              THEN ISNULL(CP.OPTIN_STATUS, 'F')
              WHEN C.DO_NOT_MAIL = 'T' THEN 'F'
              WHEN C.DO_NOT_MAIL = 'F' AND ISNULL(CP.OPTIN_STATUS, 'T') = 'T' THEN 'T'
              ELSE 'F'
              END AS Opt_In_Status
From Contacts C
       Left Join Communication_Preferences Cp
              ON C.CONTACT_ID = CP.CONTACT_ID
              WHERE C.Email IS NOT NULL
				AND FIRSTNAME IS NOT NULL
),

CTE_TF AS
       (SELECT         CONTACT_ID
       , FIRSTNAME
       , EMAIL
       , CASE WHEN COMMUNICATION_TYPE_ID IS NULL
			THEN 99
			ELSE COMMUNICATION_TYPE_ID
			END AS ListId
       , Opt_In_Status
       , CASE WHEN Opt_In_Status = 'T' THEN 1 ELSE 0 END AS Optinstatus
       FROM CTE_CP
       ),

CTE_pvt AS
(SELECT Contact_ID
       , FIRSTNAME
       , EMAIL
       , CASE WHEN [6] /*= 1 THEN 1 ELSE 0 END*/ IS NULL THEN 0 ELSE [6] END AS 'Opt-in Feedback and Input'
       , CASE WHEN [5] /*= 1 THEN 1 ELSE 0 END*/ IS NULL THEN 0 ELSE [5] END AS 'Opt-in Technical Bulletins'
       , CASE WHEN [4] /*= 1 THEN 1 ELSE 0 END*/ IS NULL THEN 0 ELSE [4] END AS 'Opt-in Release and Tax Bulletins'
       , CASE WHEN [2] /*= 1 THEN 1 ELSE 0 END*/ IS NULL THEN 0 ELSE [2] END AS 'Opt-in Cloud Bulletins'
       , CASE WHEN [1] /*= 1 THEN 0 WHEN [1] IS NULL THEN 0 ELSE 1 END*/ IS NULL THEN 0 ELSE [1] END AS 'Unsubscribe All'
	   , CASE WHEN [99] /*= 1 THEN 0 WHEN [1]*/ IS NULL THEN 0 ELSE [99] END AS 'Do Not Mail'
FROM
CTE_TF
PIVOT
(
SUM (OptInStatus)
FOR ListID IN
([6], [5], [4], [2], [1], [99])
) AS pvt),

CTE_Merge AS
(SELECT Contact_ID
	,FirstName
	,Email
	,SUM([Opt-in Feedback and Input]) AS 'Opt-in Feedback and Input'
	,SUM([Opt-in Technical Bulletins]) AS 'Opt-in Technical Bulletins'
	,SUM([Opt-in Release and Tax Bulletins]) AS 'Opt-in Release and Tax Bulletins'
	,SUM([Opt-in Cloud Bulletins]) AS 'Opt-in Cloud Bulletins'
	,(SUM([Unsubscribe All]) + SUM([Do Not Mail])) AS 'Unsubscribe All'
	--,SUM([Do Not Mail]) AS 'Do Not Mail'
	FROM CTE_pvt
	GROUP BY Contact_ID
		,EMAIL
		,FIRSTNAME
),

CTE_ReadySum AS
(SELECT CONTACT_ID
	,FIRSTNAME
	,EMAIL
	,[Opt-in Feedback and Input]
	,[Opt-in Technical Bulletins]
	,[Opt-in Release and Tax Bulletins]
	,[Opt-in Cloud Bulletins]
	,CASE WHEN [Unsubscribe All] = 1 THEN 0 ELSE 1 END AS 'Unsubscribe All'
	--,CASE WHEN [Do Not Mail] = 1 THEN 0 ELSE 1 END AS 'Do Not Mail'
	FROM CTE_Merge),

CTE_DeDup AS
(SELECT Email
		,FirstName
		,SUM([Opt-in Feedback and Input]) AS 'Opt-in Feedback and Input'
       ,SUM([Opt-in Technical Bulletins]) AS 'Opt-in Technical Bulletins'
       ,SUM([Opt-in Release and Tax Bulletins]) AS 'Opt-in Release and Tax Bulletins'
       ,SUM([Opt-in Cloud Bulletins]) AS 'Opt-in Cloud Bulletins'
	   ,SUM([Unsubscribe All]) AS 'Unsubscribe All'
		FROM CTE_ReadySum
		GROUP BY Email
			,FIRSTNAME) 

SELECT
       --CTE_Binary.CONTACT_ID
       /*,*/EMAIL
       ,FIRSTNAME
	   --,[Opt-in Feedback and Input]
	   --,[Opt-in Technical Bulletins]
	   --,[Opt-in Release and Tax Bulletins]
	   --,[Opt-in Cloud Bulletins]
	   --,[Unsubscribe All]
       ,CASE WHEN [Opt-in Feedback and Input] > 0 THEN 'TRUE'
              ELSE 'FALSE' END AS 'Opt-in Feedback and Input'
       ,CASE WHEN [Opt-in Technical Bulletins] > 0 THEN 'TRUE'
              ELSE 'FALSE' END AS 'Opt-in Technical Bulletins'
       ,CASE WHEN [Opt-in Release and Tax Bulletins] > 0 THEN 'TRUE'
              ELSE 'FALSE' END AS 'Opt-in Release and Tax Bulletins'
       ,CASE WHEN [Opt-in Cloud Bulletins] > 0 THEN 'TRUE'
              ELSE 'FALSE' END AS 'Opt-in Cloud Bulletins'
       ,CASE WHEN [Unsubscribe All] > 0 THEN 'TRUE'
              ELSE 'FALSE' END AS 'Unsubscribe All'
FROM CTE_DeDup
